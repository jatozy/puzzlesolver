# PuzzleSolver

This is a program that uses computer vision techniques to solve jigsaw puzzles using the shapes of the edges.

## IMPORTANT INFORMATION

This repository is a fork of **https://github.com/jzeimen/PuzzleSolver.git**. The original software
is written by **Joe Zeimen**. The first commit on the master of this repository contains the orignal code from his repository.

## Prerequisites

Install the following packages to build the software

```
g++ (>= 7.2.0)
make (>= 4.1)
cmake (>= 3.9.1)
opencv (>= 3.4.1)
Qt (>= 5.10.1)
```

### Linux (Tested on Ubuntu 17.10)

#### g++, make, cmake
To install and check the version of g++, make and cmake

```
sudo apt-get update
sudo apt-get install build-essential cmake
g++ --version
make --version
cmake --version
```

#### opencv
After this check if you already have installed opencv with a correct version

```
opencv_version
```

If this command does not exist, then execute the following code snippet.
It will create the folder **opencv** inside your home directory.
It will checkout opencv inside this folder.
Then it will compile opencv (This will take a while).
After this opencv will be installed.
The last step is to delete the created folder **opencv**

```
cd ~
git clone git://code.opencv.org/opencv.git
cd opencv
git checkout 3.4.1
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local ..
make -j8
sudo make install
cd ~
rm -Rvf opencv
```

#### Qt

Use the download manager from Qt [Qt 5.10.1](https://download.qt.io/official_releases/qt/5.10/5.10.1/)

## Compile

### Linux

The command to compile the software depends on the installation path of **Qt**.
So for example:
If you have installed Qt Version **5.10.1** to the folder **/opt/Qt**. Then the folder
**/opt/Qt/5.10.1** should exist.
So in this case you have to compile this software with the following code snippet

```
cd ~
git clone https://jatozy@bitbucket.org/jatozy/puzzlesolver.git
cd puzzlesolver
mkdir build
cd build
cmake -D CMAKE_PREFIX_PATH=/opt/Qt/5.10.1/gcc_64/lib/cmake ..
make
```

If you have installed Qt to another location or you have installed another version of Qt then
you need to change the content of the **CMAKE_PREFIX_PATH** to the correct location.

**Hint**
If you have given the wrong (or none) path, then cmake will give you an error message like
```
  By not providing "FindQt5.cmake" in CMAKE_MODULE_PATH this project has
  asked CMake to find a package configuration file provided by "Qt5", but
  CMake did not find one.

  Could not find a package configuration file provided by "Qt5" (requested
  version 5.10.1) with any of the following names:

    Qt5Config.cmake
    qt5-config.cmake

  Add the installation prefix of "Qt5" to CMAKE_PREFIX_PATH or set "Qt5_DIR"
  to a directory containing one of the above files.  If "Qt5" provides a
  separate development package or SDK, be sure it has been installed.


-- Configuring incomplete, errors occurred!
```
