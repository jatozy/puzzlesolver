#pragma once

#include <QObject>

class analyseScans : public QObject {
    Q_OBJECT

private slots:
    void solveWinniePooh();
    void solveToyStory();
    void solveToyStoryBackside();
    void solveAngryBirdsColor();
    void solveAngryBirdsScannerOpen();
    void solveHorses();
    void solveHorsesNumbered();

private:
    void solveAndCheckGivenPuzzle(QString pathToPictures, int estimated_piece_size, int threshold, QVector<QVector<int> > expectedSolution);
};
