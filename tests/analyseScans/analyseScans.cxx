#include "analyseScans.h"

#include <iostream>
#include <puzzle.h>
#include <QDebug>
#include <QDir>
#include <QTest>
#include <QVector>
#include <iostream>
#include <string>

QTEST_MAIN(analyseScans)

void analyseScans::solveAndCheckGivenPuzzle(QString pathToPictures, int estimated_piece_size, int threshold, QVector<QVector<int> > expectedSolution)
{
    std::string puzzlePath = pathToPictures.toStdString();
    puzzle puzzle(puzzlePath, estimated_piece_size, threshold);
    puzzle.solve();

    cv::Mat_<int> solution = puzzle.getSolution();

    std::cout<<"Solution:"<<std::endl;
    for(int i=0; i<solution.size[0];i++) {
        for(int j=0; j<solution.size[1]; j++) {
            const int solvedValue = solution(i,j);
            std::cout<<solvedValue<<",";
        }
        std::cout<<std::endl;
    }

    for(int i=0; i<solution.size[0];i++) {
        for(int j=0; j<solution.size[1]; j++) {
            const int solvedValue = solution(i,j);
            const int expectedValue = expectedSolution[i][j];
            QString message=QString("the expected value is >>%1<< but the solution value is >>%2<<").arg(expectedValue).arg(solvedValue);
            QVERIFY2(solvedValue == expectedValue, qPrintable(message));
        }
    }
}

void analyseScans::solveWinniePooh()
{
    QString curDir = QDir::currentPath();
    QString puzzlePath = curDir + "/../tests/scans/Winnie Pooh/";

    QVector<QVector<int> > expectedSolution {
        {6,12,8,7,5},
        {2,4,1,13,14},
        {11,9,12,3,0}
    };

    solveAndCheckGivenPuzzle(puzzlePath, 450, 22, expectedSolution);
}

void analyseScans::solveToyStory()
{
    QString curDir = QDir::currentPath();
    QString puzzlePath = curDir + "/../tests/scans/Toy Story/";

    QVector<QVector<int> > expectedSolution {
        {44,9,12,3,10,6,13,46},
        {43,2,23,17,28,40,34,14},
        {27,42,29,11,18,26,25,20},
        {41,31,45,16,19,0,30,15},
        {4,24,32,22,39,36,1,37},
        {35,47,21,8,38,5,7,33}
    };

    solveAndCheckGivenPuzzle(puzzlePath, 200, 22, expectedSolution);
}

void analyseScans::solveToyStoryBackside() {
    QString curDir = QDir::currentPath();
    QString puzzlePath = curDir + "/../tests/scans/Toy Story back/";

    QVector<QVector<int> > expectedSolution {
        {12,35,21,3,9,41},
        {5,22,36,15,46,6},
        {17,31,25,29,32,44},
        {34,33,42,14,1,0},
        {8,28,27,10,24,45},
        {26,20,40,7,2,11},
        {39,16,23,13,18,47},
        {43,4,30,19,37,38}
    };

    solveAndCheckGivenPuzzle(puzzlePath, 200, 50, expectedSolution);
}

void analyseScans::solveAngryBirdsColor()
{
    QString curDir = QDir::currentPath();
    QString puzzlePath = curDir + "/../tests/scans/Angry Birds/color/";

    QVector<QVector<int> > expectedSolution {
        {9,16,18,15},
        {2,17,12,23},
        {21,13,14,22},
        {20,3,11,7},
        {10,8,4,19},
        {5,1,0,6}
    };

    solveAndCheckGivenPuzzle(puzzlePath, 300, 30, expectedSolution);
}

void analyseScans::solveAngryBirdsScannerOpen()
{
    QString curDir = QDir::currentPath();
    QString puzzlePath = curDir + "/../tests/scans/Angry Birds/Scanner Open/";

    QVector<QVector<int> > expectedSolution {
        {21,4,1,23,6,9},
        {20,0,3,19,11,5},
        {10,18,16,8,2,12},
        {14,17,15,13,22,7}
    };

    solveAndCheckGivenPuzzle(puzzlePath, 300, 30, expectedSolution);
}

void analyseScans::solveHorses()
{
    QString curDir = QDir::currentPath();
    QString puzzlePath = curDir + "/../tests/scans/horses/";

    QVector<QVector<int> > expectedSolution {
        {95,77,28,27,10,26,5,42,80,55,70,81,53},
        {37,101,8,24,78,74,57,91,97,90,15,64,17},
        {19,88,25,14,32,73,29,76,103,7,3,62,60},
        {59,0,67,30,23,45,36,13,65,33,56,99,89},
        {18,11,102,54,38,84,79,87,34,51,61,98,58},
        {83,63,44,72,75,86,12,20,4,46,43,50,35},
        {16,47,100,69,52,82,66,31,9,94,85,92,71},
        {48,68,41,21,1,6,93,96,2,22,49,39,40}
    };

    solveAndCheckGivenPuzzle(puzzlePath, 380, 50, expectedSolution);
}

void analyseScans::solveHorsesNumbered()
{
    QString curDir = QDir::currentPath();
    QString puzzlePath = curDir + "/../tests/scans/horses numbered/";

    QVector<QVector<int> > expectedSolution {
        {81,59,100,93,67,12,28,72,11,61,34,26,13},
        {18,69,22,42,86,94,97,79,10,82,44,62,102},
        {95,47,25,20,46,5,75,70,31,38,60,57,73},
        {17,36,40,88,45,74,41,30,53,98,6,14,43},
        {19,32,83,103,99,96,9,78,35,77,49,37,55},
        {15,0,8,21,4,1,51,66,54,52,23,71,101},
        {63,16,90,65,29,7,2,68,85,91,84,50,76},
        {64,39,87,92,24,80,3,58,33,89,56,48,27}
    };

    solveAndCheckGivenPuzzle(puzzlePath, 200, 22, expectedSolution);
}
